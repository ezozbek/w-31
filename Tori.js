import React from "react";
import "./Tori.css";

const Tori = () => {
  return (
    <div className="tori">
      <div className="search-container">
        <input
          type="text"
          placeholder="Hakusana ja/tai postinumero"
          className="search-input bigger-search"
        />
        <select name="what" className="filter-select">
          <option id="1">Kaikki osastot</option>
        </select>
        <select name="where" className="filter-select">
          <option id="1">Koko Suomi</option>
        </select>
      </div>
      <div className="filter-checkboxes">
        <input type="checkbox" name="how" id="myydaan" />
        <label htmlFor="myydaan">Myydään</label>
        <input type="checkbox" name="how" id="ostetaan" />
        <label htmlFor="ostetaan">Ostetaan</label>
        <input type="checkbox" name="how" id="vuokrataan" />
        <label htmlFor="vuokrataan">Vuokrataan</label>
        <input type="checkbox" name="how" id="halutaan-vuokrata" />
        <label htmlFor="halutaan-vuokrata">Halutaan vuokrata</label>
        <input type="checkbox" name="how" id="annetaan" />
        <label htmlFor="annetaan">Annetaan</label>
      </div>
      <button className="search-button blue-button">Hae</button>
    </div>
  );
};

export default Tori;
